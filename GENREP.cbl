       IDENTIFICATION DIVISION. 
       PROGRAM-ID. GENREP.
       AUTHOR. kittipon thaweealarb.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION.
       FILE-CONTROL. 
           SELECT 100-INPUT-FILE ASSIGN TO "product.csv"
              ORGANIZATION IS LINE SEQUENTIAL
              FILE STATUS IS WS-INPUT-FILE-STATUS.
           SELECT 200-INPUT-FILE ASSIGN TO "transac.csv"
              ORGANIZATION IS LINE SEQUENTIAL
              FILE STATUS IS WS-200-INPUT-FILE-STATUS.
           SELECT 300-FORMAT-OUTPUT   ASSIGN TO "REPORT.rpt"
              ORGANIZATION IS LINE SEQUENTIAL
              FILE STATUS IS WS-300-OUTPUT-STATUS.


       DATA DIVISION. 
       FILE SECTION. 
       FD 100-INPUT-FILE
           BLOCK CONTAINS 0 RECORDS.
       01 100-INPUT-FILE-RECORD.
          05 100-PRODUCT-ID         PIC X(4)        VALUE SPACES.
          05 FILLER                 PIC X(1)        VALUE SPACES.
          05 100-PRODUCT-NAME       PIC X(15)       VALUE SPACES.
          05 FILLER                 PIC X(1)        VALUE SPACES.
          05 100-PRODUCT-PRICE      PIC 9V99        VALUE ZEROS.
       FD 200-INPUT-FILE
           BLOCK CONTAINS 0 RECORDS.
       01 200-INPUT-FILE-RECORD.
          05 200-TRANS-ID           PIC X(5)        VALUE SPACES.
          05 FILLER                 PIC X(1)        VALUE SPACES.
          05 200-TND-HHMN           PIC X(14)       VALUE SPACES.
          05 FILLER                 PIC X(1)        VALUE SPACES.
          05 200-PRODUCT-ID         PIC X(4)        VALUE SPACES.
          05 FILLER                 PIC X(1)        VALUE SPACES.
          05 200-QTY                PIC 9(2)        VALUE ZEROS.
       FD 300-FORMAT-OUTPUT BLOCK CONTAINS 0 RECORDS.
       01 300-OUTPUT-RECORD         PIC   X(80)     VALUE SPACES.

        WORKING-STORAGE SECTION.
       01 WS-CACULATION.
          05 WS-PRODUCT-MAX         PIC 9(3)        VALUE 5.
          05 WS-TRAN-MAX            PIC 9(3)        VALUE 50.
          05 WS-PRODUCT-CB          PIC X(4)        VALUE SPACES.
          05 WS-QTY-TOTAL           PIC 9(3)        VALUE ZEROS.
          05 WS-TOTAL               PIC   9(4)V9(2) VALUE ZEROS.
          05 WS-TRAN-QTY            PIC   9(4)V9(2) VALUE ZEROS.

       01 WS-MATCH-PRODUCT.
          05 WSM-PRODUCT-ID         PIC X(4)        VALUE SPACES.
          05 FILLER                 PIC X(1)        VALUE SPACES.
          05 WSM-PRODUCT-NAME       PIC X(15)       VALUE SPACES.
          05 FILLER                 PIC X(1)        VALUE SPACES.
          05 WSM-PRODUCT-PRICE      PIC 9V99        VALUE ZEROS.
       01 WS-INPUT-FILE-STATUS      PIC X(2).
          88 FILE-OK                                VALUE "00".
          88 FILE-AT-END                            VALUE "10".
       01 WS-200-INPUT-FILE-STATUS  PIC X(2).
          88 FILE-OK                                VALUE "00".
          88 FILE-AT-END                            VALUE "10".
       01 WS-300-OUTPUT-STATUS      PIC   X(2).
          88 FILE-IS-OK                             VALUE "00".
          88 FILE-AT-END                            VALUE "10".

       01 WS-ARRAY-PRODUCT OCCURS 5 TIMES
             INDEXED BY WS-ID-PROD.
          05 WSA-PRODUCT-ID         PIC X(4)        VALUE SPACES.
          05 FILLER                 PIC X(1)        VALUE SPACES.
          05 WSA-PRODUCT-NAME       PIC X(15)       VALUE SPACES.
          05 FILLER                 PIC X(1)        VALUE SPACES.
          05 WSA-PRODUCT-PRICE      PIC 9V99        VALUE ZEROS.
       01 WS-ARRAY-TRAN OCCURS 50 TIMES
             INDEXED BY WS-ID-TRAN.
          05 WSA-TRANS-ID           PIC X(5)        VALUE SPACES.
          05 FILLER                 PIC X(1)        VALUE SPACES.
          05 WSA-TND-HHMN           PIC X(14)       VALUE SPACES.
          05 FILLER                 PIC X(1)        VALUE SPACES.
          05 WSA-PRODUCT-ID         PIC X(4)        VALUE SPACES.
          05 FILLER                 PIC X(1)        VALUE SPACES.
          05 WSA-QTY                PIC 9(2)        VALUE ZEROS.
       01 RPT-FORMAT.
          05 RPT-HEADER.
             10 RPT-PRODUCT-ID      PIC   X(4)      VALUE SPACES.
             10 FILLER              PIC   X(4)      VALUE SPACES.
             10 RPT-PRODUCT-NAME    PIC   X(15)     VALUE SPACES.
             10 FILLER              PIC   X(5)      VALUE SPACES.
             10 RPT-PRODUCT-PRICE   PIC   $9.99     VALUE ZERO.
          05 RPT-COL-HEADER         PIC   X(33)
                                                    VALUE
                "#ID    D/M/Y HH:MN    QTY  TOTAL ".
          05 RPT-DOT-LINE.
             10 FILLER              PIC   X OCCURS 33 TIMES
                                                    VALUE ".".
          05 RPT-TRAN-DETAIL.
             10 RPT-TRAN-ID         PIC   X(5)      VALUE SPACES.
             10 FILLER              PIC   X(2)      VALUE SPACES.
             10 RPT-TRAN-YMD-HHMN   PIC   X(14)     VALUE SPACES.
             10 FILLER              PIC   X         VALUE SPACES.
             10 RPT-QTY             PIC   ZZ9       VALUE ZEROS.
             10 FILLER              PIC   X         VALUE SPACES.
             10 RPT-PRICE           PIC   $$$9.99   VALUE ZERO.
          05 RPT-FOOTER.
             10 FILLER              PIC   X(13)
                                                    VALUE
                   "        QTY =".
             10 RPT-TOTAL-QTY       PIC   ZZZ9      VALUE ZEROS.
             10 FILLER              PIC   X(7)      VALUE " TOTAL=".
             10 RPT-TOTAL           PIC  $$$$$9.99  VALUE ZEROS.
          05 RPT-DASH-LINE.       
             10 FILLER              PIC   X OCCURS 33 TIMES
                                                    VALUE "-".
  
        PROCEDURE DIVISION.
           PERFORM 1000-INITIAL THRU 1000-EXIT
           PERFORM 2000-PROCESS THRU 2000-EXIT
           PERFORM 3000-END THRU 3000-EXIT 
           GOBACK
           .

       1000-INITIAL.
           OPEN INPUT 100-INPUT-FILE 200-INPUT-FILE 
           OPEN OUTPUT 300-FORMAT-OUTPUT 

      *    read file in CSV
      *    AND load file in top array
           PERFORM 8100-READ-PRODUCT THRU 8100-EXIT
           PERFORM 4100-LOAD-PRODUCT THRU 4100-EXIT 

           PERFORM 8200-READ-TRAN THRU 8200-EXIT
           PERFORM 4200-LOAD-TRAN THRU 4200-EXIT 
           .
       1000-EXIT.
           EXIT.
           
       2000-PROCESS.
      *    DISPLAY "process"
      *    PERFORM 2910-DEBUG-PRODUCT-ARRAY THRU 2910-EXIT
      *    PERFORM 2920-DEBUG-TRAN-ARRAY THRU 2920-EXIT 
      *    .
           PERFORM VARYING WS-ID-TRAN FROM 1 BY 1
              UNTIL WS-ID-TRAN > WS-TRAN-MAX 
                   IF WS-PRODUCT-CB NOT =
                      WSA-PRODUCT-ID OF WS-ARRAY-TRAN(WS-ID-TRAN)
                      IF WS-PRODUCT-CB NOT = SPACE
                         PERFORM 2300-PROCESS-END THRU 2300-EXIT 
                      END-IF
                      PERFORM 2100-PROCESS-HEAD THRU 2100-EXIT
                   END-IF
                   PERFORM 2200-PROCESS-DETAIL THRU 2200-EXIT
           END-PERFORM
           DISPLAY "END-PRODUCT"
           .
       2000-EXIT.
           EXIT.
             
       2100-PROCESS-HEAD.
           MOVE WSA-PRODUCT-ID OF WS-ARRAY-TRAN(WS-ID-TRAN)
              TO WS-PRODUCT-CB 
           SET WS-ID-PROD TO 1
           SEARCH WS-ARRAY-PRODUCT 
           WHEN WSA-PRODUCT-ID
              OF WS-ARRAY-PRODUCT(WS-ID-PROD) = WS-PRODUCT-CB
                MOVE WS-ARRAY-PRODUCT(WS-ID-PROD)
                   TO WS-MATCH-PRODUCT
           END-SEARCH  
           MOVE WSM-PRODUCT-ID TO RPT-PRODUCT-ID
           MOVE WSM-PRODUCT-NAME TO RPT-PRODUCT-NAME
           MOVE WSM-PRODUCT-PRICE TO RPT-PRODUCT-PRICE
           DISPLAY RPT-HEADER 
           MOVE RPT-HEADER TO 300-OUTPUT-RECORD
           PERFORM 7000-WRITE THRU 7000-EXIT  
           DISPLAY RPT-COL-HEADER
           MOVE RPT-COL-HEADER TO 300-OUTPUT-RECORD 
           PERFORM 7000-WRITE THRU 7000-EXIT  
           DISPLAY RPT-DOT-LINE 
           MOVE RPT-DOT-LINE TO 300-OUTPUT-RECORD 
           PERFORM 7000-WRITE THRU 7000-EXIT 

           MOVE ZEROS TO WS-QTY-TOTAL
           .
       2100-EXIT.
           EXIT.
       2200-PROCESS-DETAIL.
      *    DISPLAY WS-ARRAY-TRAN(WS-ID-TRAN)
      *    ADD WSA-QTY(WS-ID-TRAN) TO WS-QTY-TOTAL
           COMPUTE WS-TRAN-QTY = WSA-QTY IN WS-ARRAY-TRAN(WS-ID-TRAN)
              * WSM-PRODUCT-PRICE
           MOVE WSA-TRANS-ID IN WS-ARRAY-TRAN(WS-ID-TRAN) TO RPT-TRAN-ID 
           MOVE WSA-TND-HHMN(WS-ID-TRAN) TO RPT-TRAN-YMD-HHMN 
           MOVE WSA-QTY(WS-ID-TRAN) TO RPT-QTY
           MOVE WS-TRAN-QTY TO RPT-PRICE
           DISPLAY RPT-TRAN-DETAIL
           MOVE RPT-TRAN-DETAIL TO 300-OUTPUT-RECORD 
           PERFORM 7000-WRITE THRU 7000-EXIT
           ADD WSA-QTY(WS-ID-TRAN) TO WS-QTY-TOTAL 

           .
       2200-EXIT.
           EXIT.
       2300-PROCESS-END.
      *    DISPLAY "QTY = " WS-QTY-TOTAL
      *    DISPLAY "END-PRODUCT"
           COMPUTE WS-TOTAL = WS-QTY-TOTAL * WSM-PRODUCT-PRICE
           MOVE WS-QTY-TOTAL TO RPT-TOTAL-QTY
           MOVE WS-TOTAL TO RPT-TOTAL
           DISPLAY RPT-FOOTER 
           MOVE RPT-FOOTER TO 300-OUTPUT-RECORD
           PERFORM 7000-WRITE THRU 7000-EXIT
           DISPLAY RPT-DASH-LINE 
           MOVE RPT-DASH-LINE TO 300-OUTPUT-RECORD
           PERFORM 7000-WRITE THRU 7000-EXIT
           .

       2300-EXIT.
           EXIT.

      *2910-DEBUG-PRODUCT-ARRAY.
      *    PERFORM VARYING WS-ID-PROD FROM 1 BY 1
      *       UNTIL WS-ID-PROD > WS-PRODUCT-MAX
      *            DISPLAY WSA-PRODUCT-ID
      *               OF WS-ARRAY-PRODUCT(WS-ID-PROD)
      *                    " "
      *                    WSA-PRODUCT-NAME(WS-ID-PROD)
      *                    " "
      *                    WSA-PRODUCT-PRICE(WS-ID-PROD)
      *                    " "
      *            PERFORM 8100-READ-PRODUCT THRU 8100-EXIT 
      *    END-PERFORM
      *    .
      *2910-EXIT.
      *    EXIT.

      *2920-DEBUG-TRAN-ARRAY.
      *    PERFORM VARYING WS-ID-TRAN FROM 1 BY 1
      *       UNTIL WS-ID-TRAN > WS-TRAN-MAX 
      *            DISPLAY WSA-TRANS-ID(WS-ID-TRAN)
      *                    " "
      *                    WSA-TND-HHMN(WS-ID-TRAN)
      *                    " "
      *                    WSA-PRODUCT-ID OF WS-ARRAY-TRAN(WS-ID-TRAN)
      *                    " "
      *                    WSA-QTY(WS-ID-TRAN)
      *                    " "
      *            PERFORM 8200-READ-TRAN THRU 8200-EXIT 
      *    END-PERFORM
      *    .
      *2920-EXIT.
      *    EXIT.
       
       3000-END.
           CLOSE 100-INPUT-FILE 200-INPUT-FILE 300-FORMAT-OUTPUT 
           .
       3000-EXIT.
           EXIT.

       4100-LOAD-PRODUCT.
           PERFORM VARYING WS-ID-PROD FROM 1 BY 1
              UNTIL WS-ID-PROD > WS-PRODUCT-MAX
              OR FILE-AT-END OF WS-INPUT-FILE-STATUS 
                   MOVE 100-INPUT-FILE-RECORD TO WS-ARRAY-PRODUCT
                      (WS-ID-PROD)
                   PERFORM 8100-READ-PRODUCT THRU 8100-EXIT 
           END-PERFORM
           .
       4100-EXIT.
           EXIT.
       
       4200-LOAD-TRAN.
           PERFORM VARYING WS-ID-TRAN FROM 1 BY 1
              UNTIL WS-ID-TRAN > WS-TRAN-MAX
              OR FILE-AT-END OF WS-200-INPUT-FILE-STATUS  
                   MOVE 200-INPUT-FILE-RECORD TO WS-ARRAY-TRAN
                      (WS-ID-TRAN)
                   PERFORM 8200-READ-TRAN THRU 8200-EXIT  
           END-PERFORM
           SORT WS-ARRAY-TRAN ON ASCENDING KEY WSA-PRODUCT-ID
              OF WS-ARRAY-TRAN 
           .
       4200-EXIT.
           EXIT.

       7000-WRITE.
           WRITE 300-OUTPUT-RECORD  
           .
       7000-EXIT.
           EXIT.

       8100-READ-PRODUCT.
           READ 100-INPUT-FILE 
           .
       8100-EXIT.
           EXIT.

       8200-READ-TRAN.
           READ 200-INPUT-FILE 
           .
       8200-EXIT.
           EXIT.